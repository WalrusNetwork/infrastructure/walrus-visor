package network.walrus.visor.checker;

import network.walrus.visor.WalrusVisor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Inactivity Checker
 *
 * @author MSH
 */
public class InactivityChecker implements Listener {
    private Map<UUID, Instant> activityStamps = new HashMap<>();


    public InactivityChecker() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(WalrusVisor.get(), () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                Instant playerStamp = activityStamps.getOrDefault(player.getUniqueId(), Instant.now());
                Instant compare = Instant.now();
                Duration between = Duration.between(compare, playerStamp);
                if (between.getSeconds() >= 900) {
                    isAfk(player);
                }
            }
        }, 0L, 1200L);
    }

    private void isAfk(Player player) {
        player.kickPlayer("You've been idle for too long!");
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        playerIsAlive(event.getPlayer());
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        playerIsAlive(event.getPlayer());
    }

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        playerIsAlive(event.getPlayer());
    }

    private void playerIsAlive(Player player) {
        this.activityStamps.put(player.getUniqueId(), Instant.now());
    }
}
