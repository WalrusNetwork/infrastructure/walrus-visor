package network.walrus.visor;

import network.walrus.visor.checker.InactivityChecker;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class WalrusVisor extends JavaPlugin {
    private static WalrusVisor instance;
    private InactivityChecker inactivityChecker;

    public static WalrusVisor get() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        System.out.println("Visor enabled!");
        inactivityChecker = new InactivityChecker();
        registerListener(inactivityChecker);
    }

    public void registerListener(Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener, this);
    }

    public void unregisterListener(Listener listener) {
        HandlerList.unregisterAll(listener);
    }
}
